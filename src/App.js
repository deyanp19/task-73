import "./App.css";
 
import {useState,useEffect} from 'react';

function App() {
  const [count, setCount] = useState(1);

  function handleClick() {
    setCount(oldState=>oldState + 1);
  }

  useEffect(() => {
    console.log('use effect');
     
    document.title=`Count (${count})`  
     
  }, [count])

  return (
    <div className="App">
      <section class="hero">
        <div class="hero-body">
          <p class="title">A React Task</p>
          <p class="subtitle">by Boom.dev</p>
        </div>
      </section>
      <div class="container is-fullhd">
        <div class="notification">
          Edit the <code>./src</code> folder to add components.
        </div>
        <button onClick={handleClick}>
            Count ({count}) 
        </button>
      </div>
    </div>
  );
}
 
export default App;
